# -*- coding: utf-8 -*-
from sudoku import *
class Solver:
    def __init__(self, sudoku):
        assert type(sudoku) is Sudoku
        if sudoku.checkCorrectness() == False:
            raise Exception("Wrong sudoku board!")
        self.__sudoku = sudoku
        self.__moves = []
    def findFieldToFill(self):
        allPossibilities = []
        # list member - (x, y, number of possibilities)
        for x in range(9):
            for y in range(9):
                if self.__sudoku.isEmpty(x, y):
                    allPossibilities.append((x, y, len(self.__sudoku.checkPossibilities(x, y))))
        if len(allPossibilities) == 0:
            return None
        return min(allPossibilities, key = lambda possibility: possibility[2])[0 : 2]
    def __nextMove(self):
        pos = self.findFieldToFill()
        if pos is None:
            return
        if not self.__sudoku.checkPossibilities(*pos):
            while self.__moves:
                last = self.__moves.pop()
                x, y, index = last[0], last[1], last[2]
                self.__sudoku.setField(x, y, 0)
                possibilities = self.__sudoku.checkPossibilities(x, y)
                if index < len(possibilities) - 1:
                    index += 1
                    self.__sudoku.setField(x, y, possibilities[index])
                    self.__moves.append((x, y, index))
                    return True
            return False
        else:
            x, y = pos[0], pos[1]
            self.__moves.append((x, y, 0))
            self.__sudoku.setField(x, y, self.__sudoku.checkPossibilities(x, y)[0])
            return True
        
            
    def solve(self):
        while self.__sudoku.getFieldsLeft() > 0 and self.__nextMove():
            pass
    
    def printResult(self):
        self.__sudoku.printBoard()