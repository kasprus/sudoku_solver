# -*- coding: utf-8 -*-

class Sudoku:
    def __init__(self):
        self.__board = [[0] * 9 for  i in range(9)]
    def checkCorrectness(self):
        for x in range(9):
            for y in range(9):
                if not self.__checkFieldCorrectness(x, y):
                    return False
        return True
    def __checkFieldCorrectness(self, x, y):
        assert x in range(9) and y in range(9)
        valuePresence = [0] * 10
        if self.__board[x][y] == 0:
            return True
        tmpList = self.getRow(x) + self.getColumn(y) + self.getRectangle(x, y)
        for field in tmpList:
            valuePresence[field] += 1
            if field != 0 and field == self.__board[x][y] and valuePresence[field] == 4:
                return False
        return True
    def checkPossibilities(self, x, y):
        assert x in range(9) and y in range(9) and self.__board[x][y] == 0
        return list(set(range(1, 10)).difference(self.getRow(x) + self.getColumn(y) + self.getRectangle(x, y)))
    def getRow(self, x):
        assert x in range(9)
        return [field for field in self.__board[x]]
    def getColumn(self, y):
        assert y in range(9)
        return [self.__board[x][y] for x in range(9)]
    def getRectangle(self, x, y):
        assert x in range(9) and y in range(9)
        x = x // 3
        y = y // 3
        return [self.__board[3 * x + j][3 * y + i] for j in range(3) for i in range(3)]
    def setField(self, x, y, value):
        assert x in range(9) and y in range(9) and value in range(0, 10)
        self.__board[x][y] = value
    def getFieldsLeft(self):
        ret = 0
        for line in self.__board:
            ret += line.count(0)
        return ret
    def printBoard(self):
        for line in self.__board:
            print(line)
    def isEmpty(self, x, y):
        assert x in range(9) and y in range(9)
        return self.__board[x][y] == 0
    def getField(self, x, y):
        assert x in range(9) and y in range(9)
        return self.__board[x][y]