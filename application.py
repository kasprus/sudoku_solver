# -*- coding: utf-8 -*-

import tkinter as tk
from sudoku import Sudoku
from solver import Solver

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.optionList = ["-"] + [str(i) for i in range(1, 10)]
        self.choices = [[tk.StringVar(self, self.optionList[0]) for j in range(9)] for i in range(9)]
        #print(self.optionList)
        #tk.StringVar(self, optionList[0])
        self.lists = [[tk.OptionMenu(self, self.choices[i][j], *self.optionList) for j in range(9)] for i in range(9)]
        for x, row in enumerate(self.lists):
            for y, field in enumerate(row):
                field.grid(row = x, column = y)
        self.solveButton = tk.Button(self, text = "Solve", command = self.__solveSudoku)
        self.resetButton = tk.Button(self, text = "Reset", command = self.__resetGame)
        self.solveButton.grid(column = 3, row = 9)
        self.resetButton.grid(column = 5, row = 9)

    def __resetGame(self):
        for row in self.choices:
            for field in row:
                field.set(self.optionList[0])

    def __solveSudoku(self):
        a = Sudoku()
        for x in range(9):
            for y in range(9):
                a.setField(x, y, 0 if self.choices[x][y].get() == "-" else int(self.choices[x][y].get()))
        Solver(a).solve()
        for x in range(9):
            for y in range(9):
                self.choices[x][y].set(self.optionList[a.getField(x, y)])
