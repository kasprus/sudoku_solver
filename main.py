# -*- coding: utf-8 -*-
from sudoku import Sudoku
from solver import Solver
from application import Application
import tkinter as tk

if __name__ == "__main__":
    root = tk.Tk()
    app = Application(master=root)
    app.mainloop()
